const Admin = require("../models/Admin");
const User = require("../models/User");
const app = require("../app");

const supertest = require("supertest");
const request = supertest(app);
const verifyJWT = require("../routes");

describe("Testing all routes", () => {
	let currentToken;

	test("Check if verifyJWT is valid", () => {
		expect(verifyJWT).toBeDefined();
	});

	test("Should return list of all admins", async (done) => {
		const res = await request.get("/api/admin");

		expect(res.status).toBe(200);
		expect(res.body.length).toBeGreaterThan(1);
		done();
	});

	test("Should call Login", async (done) => {
		const res = await request.post("/api/login").send({
			login: "ander",
			password: "123",
		});

		expect(res.status).toBe(200);
		expect(res.body).toHaveProperty("auth");
		expect(res.body).toHaveProperty("token");
		expect(res.body.auth).toBe(true);
		expect(res.body.token).toHaveLength(171);
		currentToken = res.body.token;
		done();
	});

	test("Should call Login with failed", async (done) => {
		const res = await request.post("/api/login").send({
			login: "anderson",
			password: "123456",
		});

		expect(res.status).toBe(500);
		done();
	});

	test("Should call Logout", async (done) => {
		const res = await request.get("/api/logout");

		expect(res.status).toBe(200);
		expect(res.body).toHaveProperty("auth");
		expect(res.body).toHaveProperty("token");
		expect(res.body.auth).toBe(false);
		expect(res.body.token).toBe(null);
		done();
	});

	test("Should call Users with failed", async (done) => {
		const res = await request.get("/api/users");
		const text = JSON.parse(res.text);

		expect(res.status).toBe(401);
		expect(text).toHaveProperty("auth");
		expect(text).toHaveProperty("message");
		expect(text.auth).toBe(false);
		expect(text.message).toBe("No token provided");
		done();
	});

	test("Should call Users", async (done) => {
		const res = await request.get("/api/users").set("x-access-token", currentToken);

		expect(res.status).toBe(200);
		expect(res.body.length).toBeGreaterThan(1);
		done();
	});
});

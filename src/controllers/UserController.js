const mongoose = require("mongoose");
const User = mongoose.model("User");

module.exports = {
	async getAll(req, res) {
		const items = await User.find({});
		return res.json(items);
	},
	async getByID(req, res) {
		const item = await User.findById(req.params.id);
		return res.json(item);
	},
	async incluir(req, res) {
		try {
			const item = await User.create(req.body);
			req.io.emit("newUser", item);
			return res.json(item);
		} catch (e) {
			const item = await User.find({ email: req.body.email });
			return res.status(404).send({ error: e.errmsg, idUser: item[0]._id });
		}
	},
	async editar(req, res) {
		try {
			const item = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
			req.io.emit("editUser", item);
			return res.json(item);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg, idUser: req.params.id });
		}
	},
	async excluir(req, res) {
		const item = await User.findById(req.params.id);
		req.io.emit("delUser", item);
		await User.findByIdAndRemove(req.params.id);
		return res.send();
	},
};

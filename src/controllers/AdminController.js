const mongoose = require("mongoose");
const Admin = mongoose.model("Admin");

require("dotenv").config();
var jwt = require("jsonwebtoken");

module.exports = {
	async login(req, res) {
		const item = await Admin.findOne({ login: req.body.login, password: req.body.password });

		if (item) {
			var token = jwt.sign({ id: item._id }, process.env.myTOKEN, {
				expiresIn: 60 * 60 * 1, // 1h
			});

			return res.status(200).send({ auth: true, token: token });
		}

		return res.status(500).send({ error: "Login inválido!" });
	},

	async logout(req, res) {
		res.status(200).send({ auth: false, token: null });
	},

	async getAll(req, res) {
		const items = await Admin.find({});
		return res.json(items);
	},
	async getByID(req, res) {
		const item = await Admin.findById(req.params.id);
		return res.json(item);
	},
	async incluir(req, res) {
		const item = await Admin.create(req.body);
		return res.json(item);
	},
	async editar(req, res) {
		const item = await Admin.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.json(item);
	},
	async excluir(req, res) {
		await Admin.findByIdAndRemove(req.params.id);
		return res.send();
	},
};

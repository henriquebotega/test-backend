const express = require("express");
const cors = require("cors");
const requireDir = require("require-dir");
const mongoose = require("mongoose");
const http = require("http");
const socket = require("socket.io");
const helmet = require("helmet");

mongoose.connect("mongodb+srv://usuario:senha@banco-ah7ab.mongodb.net/test?retryWrites=true&w=majority", { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = http.createServer(app);
const io = socket(server);

app.use((req, res, next) => {
	req.io = io;
	return next();
});

requireDir("./models");
app.use("/api", require("./routes"));

module.exports = server;

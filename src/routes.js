require("dotenv-safe").config({ path: __dirname + "/.env" });
const express = require("express");
const routes = express.Router();
const jwt = require("jsonwebtoken");

const verifyJWT = (req, res, next) => {
	var token = req.headers["x-access-token"];

	if (!token) {
		return res.status(401).send({ auth: false, message: "No token provided" });
	}

	jwt.verify(token, process.env.myTOKEN, (error, decoded) => {
		if (error) {
			return res.status(500).send({ auth: false, message: "Failed to authenticate token" });
		}

		req.userId = decoded.id;
		next();
	});
};

const AdminController = require("./controllers/AdminController");
routes.get("/admin", AdminController.getAll);
routes.get("/admin/:id", AdminController.getByID);
routes.post("/admin", AdminController.incluir);
routes.put("/admin/:id", AdminController.editar);
routes.delete("/admin/:id", AdminController.excluir);

routes.post("/login", AdminController.login);
routes.get("/logout", AdminController.logout);

const UserController = require("./controllers/UserController");
routes.get("/users", verifyJWT, UserController.getAll);
routes.get("/users/:id", verifyJWT, UserController.getByID);
routes.post("/users", verifyJWT, UserController.incluir);
routes.put("/users/:id", verifyJWT, UserController.editar);
routes.delete("/users/:id", verifyJWT, UserController.excluir);

module.exports = routes;

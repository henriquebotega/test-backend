const mongoose = require("mongoose");
const Admin = require("../models/Admin");
const User = require("../models/User");

const app = require("../app");
const axios = require("axios");

describe("Testing user actions", () => {
	let listener;
	let token;
	let currentUser;

	const httpServer = async () => {
		const listener = app.listen(3333);
		const port = listener.address().port;
		const baseURL = `http://localhost:${port}/api`;
		axios.defaults.baseURL = baseURL;

		return listener;
	};

	beforeAll(async () => {
		await mongoose.connect("mongodb+srv://usuario:senha@banco-ah7ab.mongodb.net/test?retryWrites=true&w=majority", { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
		listener = await httpServer();

		const res = await axios.post("/login", { login: "ander", password: "123" });
		token = res.data.token;
	});

	afterAll(async function () {
		listener.close();
	});

	test("Should be able to list all Users", async () => {
		const res = await axios.get("/users", { headers: { "x-access-token": token } });
		const nToken = res.config.headers["x-access-token"];

		expect(res.status).toBe(200);
		expect(nToken).toHaveLength(171);
	});

	test("Should be able to create a new User", async () => {
		const newUser = {
			name: "teste",
			email: "teste@servidor.com",
		};

		try {
			const res = await axios.post("/users", newUser, { headers: { "x-access-token": token } });

			expect(res.status).toBe(200);
			expect(res.data).toHaveProperty("_id");
			currentUser = res.data._id;
		} catch (e) {
			expect(e.response.status).toBe(404);
			expect(e.response.data).toHaveProperty("error");
			expect(e.response.data).toHaveProperty("idUser");
			currentUser = e.response.data.idUser;
		}
	});

	test("Should be able to edit a User by ID", async () => {
		const editUser = {
			name: "teste teste",
		};

		const res = await axios.put("/users/" + currentUser, editUser, { headers: { "x-access-token": token } });

		expect(res.status).toBe(200);
		expect(res.data).toHaveProperty("_id");
		expect(res.data.name).toBe(editUser.name);
	});

	test("Should be able to get a User by ID", async () => {
		const res = await axios.get("/users/" + currentUser, { headers: { "x-access-token": token } });

		expect(res.status).toBe(200);
		expect(res.data).toHaveProperty("_id");
		expect(res.data._id).toBe(currentUser);
	});

	test("Should be able to remove a User by ID", async () => {
		const res = await axios.delete("/users/" + currentUser, { headers: { "x-access-token": token } });

		expect(res.status).toBe(200);
	});
});

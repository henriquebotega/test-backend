const mongoose = require("mongoose");

const AdminSchema = new mongoose.Schema(
	{
		login: { type: String, require: true },
		password: { type: String, require: true },
	},
	{ timestamp: true }
);

module.exports = mongoose.model("Admin", AdminSchema);

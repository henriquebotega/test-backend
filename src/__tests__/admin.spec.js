const mongoose = require("mongoose");
const Admin = require("../models/Admin");
const User = require("../models/User");

const app = require("../app");
const axios = require("axios");

describe("Testing Admin actions", () => {
	let listener;
	let currentAdmin;

	const httpServer = async () => {
		const listener = app.listen(3333);
		const port = listener.address().port;
		const baseURL = `http://localhost:${port}/api`;
		axios.defaults.baseURL = baseURL;

		return listener;
	};

	beforeAll(async () => {
		await mongoose.connect("mongodb+srv://usuario:senha@banco-ah7ab.mongodb.net/test?retryWrites=true&w=majority", { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
		listener = await httpServer();
	});

	afterAll(async function () {
		listener.close();
	});

	test("Should be able to list all Admin", async () => {
		const res = await axios.get("/admin");
		expect(res.status).toBe(200);
	});

	test("Should be able to create a new Admin", async () => {
		const newAdmin = {
			login: "teste",
			password: "123456",
		};

		const res = await axios.post("/admin", newAdmin);

		expect(res.status).toBe(200);
		expect(res.data).toHaveProperty("_id");
		currentAdmin = res.data._id;
	});

	test("Should be able to get a Admin by ID", async () => {
		const res = await axios.get("/admin/" + currentAdmin);

		expect(res.status).toBe(200);
		expect(res.data).toHaveProperty("_id");
		expect(res.data._id).toBe(currentAdmin);
	});

	test("Should be able to edit a Admin by ID", async () => {
		const editAdmin = {
			password: "123456789",
		};

		const res = await axios.put("/admin/" + currentAdmin, editAdmin);

		expect(res.status).toBe(200);
		expect(res.data).toHaveProperty("_id");
		expect(res.data.password).toBe(editAdmin.password);
	});

	test("Should be able to remove a Admin by ID", async () => {
		const res = await axios.delete("/admin/" + currentAdmin);
		expect(res.status).toBe(200);
	});
});

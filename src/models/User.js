const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
	{
		name: { type: String },
		email: { type: String, require: true, unique: true },
	},
	{ timestamp: true }
);

module.exports = mongoose.model("User", UserSchema);
